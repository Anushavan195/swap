package swap

import (
	"crypto/ecdsa"
	"math/big"
	"sync"

	"github.com/ethereum/go-ethereum/accounts/abi"

	ethcom "github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/jinzhu/gorm"

	"github.com/binance-chain/bsc-eth-swap/common"
	"github.com/binance-chain/bsc-eth-swap/util"
)

const (
	SwapTokenReceived common.SwapStatus = "received"
	SwapQuoteRejected common.SwapStatus = "rejected"
	SwapConfirmed     common.SwapStatus = "confirmed"
	SwapSending       common.SwapStatus = "sending"
	SwapSent          common.SwapStatus = "sent"
	SwapSendFailed    common.SwapStatus = "sent_fail"
	SwapSuccess       common.SwapStatus = "sent_success"

	SwapPairReceived   common.SwapPairStatus = "received"
	SwapPairConfirmed  common.SwapPairStatus = "confirmed"
	SwapPairSending    common.SwapPairStatus = "sending"
	SwapPairSent       common.SwapPairStatus = "sent"
	SwapPairSendFailed common.SwapPairStatus = "sent_fail"
	SwapPairSuccess    common.SwapPairStatus = "sent_success"
	SwapPairFinalized  common.SwapPairStatus = "finalized"

	RetrySwapConfirmed  common.RetrySwapStatus = "confirmed"
	RetrySwapSending    common.RetrySwapStatus = "sending"
	RetrySwapSent       common.RetrySwapStatus = "sent"
	RetrySwapSendFailed common.RetrySwapStatus = "sent_fail"
	RetrySwapSuccess    common.RetrySwapStatus = "sent_success"

	SwapBsc2AVAX common.SwapDirection = "bsc_avax"
	SwapAVAX2Bsc common.SwapDirection = "avax_bsc"

	BatchSize                = 50
	TrackSentTxBatchSize     = 100
	SleepTime                = 5
	SwapSleepSecond          = 2
	TrackSwapPairSMBatchSize = 5

	TxFailedStatus = 0x00

	MaxUpperBound = "999999999999999999999999999999999999"
)

var bscClientMutex sync.RWMutex
var avaxClientMutex sync.RWMutex

type SwapEngine struct {
	mutex    sync.RWMutex
	db       *gorm.DB
	hmacCKey string
	config   *util.Config
	// key is the avax contract addr
	swapPairsFromERC20Addr map[ethcom.Address]*SwapPairIns
	bscClient              *ethclient.Client
	avaxClient             *ethclient.Client
	bscPrivateKey          *ecdsa.PrivateKey
	avaxPrivateKey         *ecdsa.PrivateKey
	bscChainID             int64
	avaxChainID            int64
	bep20ToERC20           map[ethcom.Address]ethcom.Address
	erc20ToBEP20           map[ethcom.Address]ethcom.Address

	bscSwapAgentABI  *abi.ABI
	avaxSwapAgentABI *abi.ABI

	bscSwapAgent  ethcom.Address
	avaxSwapAgent ethcom.Address
}

type SwapPairEngine struct {
	mutex   sync.RWMutex
	db      *gorm.DB
	hmacKey string
	config  *util.Config

	swapEngine *SwapEngine

	avaxClient       *ethclient.Client
	avaxPrivateKey   *ecdsa.PrivateKey
	avaxChainID      int64
	avaxTxSender     ethcom.Address
	avaxSwapAgent    ethcom.Address
	avaxSwapAgentABi *abi.ABI
}

type SwapPairIns struct {
	Symbol     string
	Name       string
	Decimals   int
	LowBound   *big.Int
	UpperBound *big.Int

	BEP20Addr ethcom.Address
	ERC20Addr ethcom.Address
}
