package swap

import (
	"context"
	"github.com/binance-chain/bsc-eth-swap/model"
	ethcom "github.com/ethereum/go-ethereum/common"
	"math/big"
)

type Server struct {
	SwapEngine *SwapEngine
}

func (s *Server) AddSwapPairInstance(ctx context.Context, in *SwapPairRequest) (*AddSwapPairResponse, error) {
	swapPair := model.SwapPair{
		Sponsor:    in.GetSponsor(),
		Symbol:     in.GetSymbol(),
		Name:       in.GetName(),
		Decimals:   int(in.GetDecimals()),
		BEP20Addr:  in.GetBEP20Addr(),
		ERC20Addr:  in.GetERC20Addr(),
		Available:  in.GetAvailable(),
		LowBound:   in.GetLowBound(),
		UpperBound: in.GetUpperBound(),
		IconUrl:    in.GetIconUrl(),
	}

	err := s.SwapEngine.AddSwapPairInstance(&swapPair)
	if err != nil {
		return new(AddSwapPairResponse), err
	}
	return new(AddSwapPairResponse), nil
}

func (s *Server) GetSwapPairInstance(ctx context.Context, in *GetSwapPairRequest) (*GetSwapPairResponse, error) {
	result, err := s.SwapEngine.GetSwapPairInstance(ethcom.HexToAddress(in.Erc20Addr))

	if err != nil {
		return new(GetSwapPairResponse), err
	}

	return &GetSwapPairResponse{
		Symbol:     result.Symbol,
		Name:       result.Name,
		Decimals:   int64(result.Decimals),
		LowBound:   result.LowBound.Bytes(),
		UpperBound: result.UpperBound.Bytes(),
		BEP20Addr:  result.BEP20Addr.Bytes(),
		ERC20Addr:  result.ERC20Addr.Bytes(),
	}, nil
}

func (s *Server) UpdateSwapInstance(ctx context.Context, in *SwapPairRequest) (*Empty, error) {
	swapPair := model.SwapPair{
		Sponsor:    in.GetSponsor(),
		Symbol:     in.GetSymbol(),
		Name:       in.GetName(),
		Decimals:   int(in.GetDecimals()),
		BEP20Addr:  in.GetBEP20Addr(),
		ERC20Addr:  in.GetERC20Addr(),
		Available:  in.GetAvailable(),
		LowBound:   in.GetLowBound(),
		UpperBound: in.GetUpperBound(),
		IconUrl:    in.GetIconUrl(),
		RecordHash: in.GetRecordHash(),
	}
	s.SwapEngine.UpdateSwapInstance(&swapPair)

	return new(Empty), nil
}

func (s *Server) InsertRetryFailedSwaps(ctx context.Context, in *InsertRetryFailedSwapsRequest) (*InsertRetryFailedSwapsResponse, error) {
	swaps, i, err := s.SwapEngine.InsertRetryFailedSwaps(in.SwapIDList)

	if err != nil {
		return new(InsertRetryFailedSwapsResponse), err
	}

	return &InsertRetryFailedSwapsResponse{
		List:         swaps,
		RejectedList: i,
	}, nil
}

func (s *Server) WithdrawToken(ctx context.Context, in *WithdrawTokenRequest) (*WithdrawTokenResponse, error) {
	bigInt := new(big.Int)
	bigInt.SetBytes(in.GetAmount())
	token, err := s.SwapEngine.WithdrawToken(in.GetChain(), ethcom.HexToAddress(in.GetTokenAddr()), ethcom.HexToAddress(in.GetRecipient()), bigInt)
	if err != nil {
		return new(WithdrawTokenResponse), err
	}

	return &WithdrawTokenResponse{
		Hash: token,
	}, nil

}
