package util

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
	"io/ioutil"
	"strconv"

	ethcom "github.com/ethereum/go-ethereum/common"

	"github.com/binance-chain/bsc-eth-swap/common"
)

type Config struct {
	KeyManagerConfig KeyManagerConfig
	DBConfig         DBConfig
	ChainConfig      ChainConfig `json:"chain_config"`
	LogConfig        LogConfig   `json:"log_config"`
	AlertConfig      AlertConfig `json:"alert_config"`
	AdminConfig      AdminConfig `json:"admin_config"`
}

func (cfg *Config) Validate() {
	cfg.DBConfig.Validate()
	cfg.ChainConfig.Validate()
	cfg.LogConfig.Validate()
	cfg.AlertConfig.Validate()
}

type AlertConfig struct {
	TelegramBotId  string `json:"telegram_bot_id"`
	TelegramChatId string `json:"telegram_chat_id"`

	BlockUpdateTimeout int64 `json:"block_update_timeout"`
}

func (cfg AlertConfig) Validate() {
	if cfg.BlockUpdateTimeout <= 0 {
		panic(fmt.Sprintf("block_update_timeout should be larger than 0"))
	}
}

type KeyManagerConfig struct {
	KeyType       string
	AWSRegion     string
	AWSSecretName string
	// local keys
	LocalHMACKey        string
	LocalAVAXPrivateKey string
	LocalBSCPrivateKey  string
	LocalAdminApiKey    string
	LocalAdminSecretKey string
}

type KeyConfig struct {
	HMACKey        string `json:"hmac_key"`
	AVAXPrivateKey string `json:"avax_private_key"`
	BSCPrivateKey  string `json:"bsc_private_key"`
	AdminApiKey    string `json:"admin_api_key"`
	AdminSecretKey string `json:"admin_secret_key"`
}

func (cfg KeyManagerConfig) Validate() {
	if cfg.KeyType == common.LocalPrivateKey && len(cfg.LocalHMACKey) == 0 {
		panic("missing local hmac key")
	}
	if cfg.KeyType == common.LocalPrivateKey && len(cfg.LocalAVAXPrivateKey) == 0 {
		panic("missing local bsc private key")
	}
	if cfg.KeyType == common.LocalPrivateKey && len(cfg.LocalBSCPrivateKey) == 0 {
		panic("missing local bsc private key")
	}

	if cfg.KeyType == common.LocalPrivateKey && len(cfg.LocalAdminApiKey) == 0 {
		panic("missing local admin api key")
	}

	if cfg.KeyType == common.LocalPrivateKey && len(cfg.LocalAdminSecretKey) == 0 {
		panic("missing local admin secret key")
	}

	if cfg.KeyType == common.AWSPrivateKey && (cfg.AWSRegion == "" || cfg.AWSSecretName == "") {
		panic("Missing aws key region or name")
	}
}

type TokenSecretKey struct {
	Symbol         string `json:"symbol"`
	AVAXPrivateKey string `json:"avax_private_key"`
	BSCPrivateKey  string `json:"bsc_private_key"`
}

type DBConfig struct {
	DbDriver   string
	DbUser     string
	DbPassword string
	DbName     string
	DbHost     string
	DbPort     int
}

func (cfg DBConfig) Validate() {
	if cfg.DbDriver != common.DBDialectMysql && cfg.DbDriver != common.DBDialectSqlite3 && cfg.DbDriver != common.DBDialectPostgresql {
		panic(fmt.Sprintf("only %s and %s and %s supported", common.DBDialectMysql, common.DBDialectSqlite3, common.DBDialectPostgresql))
	}

	if cfg.DbUser == "" {
		panic("db_user should not be empty")
	}

	if cfg.DbPassword == "" {
		panic("db_password should not be empty")
	}

	if cfg.DbName == "" {
		panic("db_name should not be empty")
	}

	if cfg.DbHost == "" {
		panic("db_host should not be empty")
	}

	if strconv.Itoa(cfg.DbPort) == "" {
		panic("db_port should not be empty")
	}

}

type ChainConfig struct {
	BalanceMonitorInterval int64 `json:"balance_monitor_interval"`

	AVAXObserverFetchInterval    int64  `json:"avax_observer_fetch_interval"`
	AVAXStartHeight              int64  `json:"avax_start_height"`
	AVAXProvider                 string `json:"avax_provider"`
	AVAXConfirmNum               int64  `json:"avax_confirm_num"`
	AVAXSwapAgentAddr            string `json:"avax_swap_agent_addr"`
	AVAXExplorerUrl              string `json:"avax_explorer_url"`
	AVAXMaxTrackRetry            int64  `json:"avax_max_track_retry"`
	AVAXAlertThreshold           string `json:"avax_alert_threshold"`
	AVAXWaitMilliSecBetweenSwaps int64  `json:"avax_wait_milli_sec_between_swaps"`

	BSCObserverFetchInterval    int64  `json:"bsc_observer_fetch_interval"`
	BSCStartHeight              int64  `json:"bsc_start_height"`
	BSCProvider                 string `json:"bsc_provider"`
	BSCConfirmNum               int64  `json:"bsc_confirm_num"`
	BSCSwapAgentAddr            string `json:"bsc_swap_agent_addr"`
	BSCExplorerUrl              string `json:"bsc_explorer_url"`
	BSCMaxTrackRetry            int64  `json:"bsc_max_track_retry"`
	BSCAlertThreshold           string `json:"bsc_alert_threshold"`
	BSCWaitMilliSecBetweenSwaps int64  `json:"bsc_wait_milli_sec_between_swaps"`
}

func (cfg ChainConfig) Validate() {
	if cfg.AVAXStartHeight < 0 {
		panic("avax_start_height should not be less than 0")
	}
	if cfg.AVAXProvider == "" {
		panic("avax_provider should not be empty")
	}
	if cfg.AVAXConfirmNum <= 0 {
		panic("avax_confirm_num should be larger than 0")
	}
	if !ethcom.IsHexAddress(cfg.AVAXSwapAgentAddr) {
		panic(fmt.Sprintf("invalid avax_swap_contract_addr: %s", cfg.AVAXSwapAgentAddr))
	}
	if cfg.AVAXMaxTrackRetry <= 0 {
		panic("avax_max_track_retry should be larger than 0")
	}

	if cfg.BSCStartHeight < 0 {
		panic("bsc_start_height should not be less than 0")
	}
	if cfg.BSCProvider == "" {
		panic("bsc_provider should not be empty")
	}
	if !ethcom.IsHexAddress(cfg.BSCSwapAgentAddr) {
		panic(fmt.Sprintf("invalid bsc_swap_contract_addr: %s", cfg.BSCSwapAgentAddr))
	}
	if cfg.BSCConfirmNum <= 0 {
		panic("bsc_confirm_num should be larger than 0")
	}
	if cfg.BSCMaxTrackRetry <= 0 {
		panic("bsc_max_track_retry should be larger than 0")
	}
}

type LogConfig struct {
	Level                        string `json:"level"`
	Filename                     string `json:"filename"`
	MaxFileSizeInMB              int    `json:"max_file_size_in_mb"`
	MaxBackupsOfLogFiles         int    `json:"max_backups_of_log_files"`
	MaxAgeToRetainLogFilesInDays int    `json:"max_age_to_retain_log_files_in_days"`
	UseConsoleLogger             bool   `json:"use_console_logger"`
	UseFileLogger                bool   `json:"use_file_logger"`
	Compress                     bool   `json:"compress"`
}

func (cfg LogConfig) Validate() {
	if cfg.UseFileLogger {
		if cfg.Filename == "" {
			panic("filename should not be empty if use file logger")
		}
		if cfg.MaxFileSizeInMB <= 0 {
			panic("max_file_size_in_mb should be larger than 0 if use file logger")
		}
		if cfg.MaxBackupsOfLogFiles <= 0 {
			panic("max_backups_off_log_files should be larger than 0 if use file logger")
		}
	}
}

type AdminConfig struct {
	ListenAddr string `json:"listen_addr"`
}

func ParseConfigFromFile(filePath string) *Config {
	viper.AddConfigPath(".")
	viper.SetConfigName("app")
	viper.SetConfigType("env")
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	bz, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(err)
	}

	var config Config
	// SET DB configs
	config.DBConfig.DbDriver = viper.GetString("DB_DRIVER")
	config.DBConfig.DbUser = viper.GetString("DB_USER")
	config.DBConfig.DbPassword = viper.GetString("DB_PASSWORD")
	config.DBConfig.DbName = viper.GetString("DB_NAME")
	config.DBConfig.DbHost = viper.GetString("DB_HOST")
	config.DBConfig.DbPort = viper.GetInt("DB_PORT")

	//SET private keys config

	config.KeyManagerConfig.KeyType = viper.GetString("KEY_TYPE")
	config.KeyManagerConfig.AWSRegion = viper.GetString("AWS_REGION")
	config.KeyManagerConfig.AWSSecretName = viper.GetString("AWS_SECRET_NAME")
	config.KeyManagerConfig.LocalHMACKey = viper.GetString("LOCAL_HMAC_KEY")
	config.KeyManagerConfig.LocalAVAXPrivateKey = viper.GetString("LOCAL_AVAX_PRIVATE_KEY")
	config.KeyManagerConfig.LocalBSCPrivateKey = viper.GetString("LOCAL_BSC_PRIVATE_KEY")

	if err := json.Unmarshal(bz, &config); err != nil {
		panic(err)
	}
	return &config
}

func ParseConfigFromJson(content string) *Config {
	var config Config
	if err := json.Unmarshal([]byte(content), &config); err != nil {
		panic(err)
	}
	return &config
}
