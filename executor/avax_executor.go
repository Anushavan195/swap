package executor

import (
	"context"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	ethcmm "github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"

	agent "github.com/binance-chain/bsc-eth-swap/abi"
	contractabi "github.com/binance-chain/bsc-eth-swap/abi"
	"github.com/binance-chain/bsc-eth-swap/common"
	"github.com/binance-chain/bsc-eth-swap/util"
)

type AvaxExecutor struct {
	Chain  string
	Config *util.Config

	SwapAgentAddr     ethcmm.Address
	AVAXSwapAgentInst *contractabi.ETHSwapAgent
	SwapAgentAbi      abi.ABI
	Client            *ethclient.Client
}

func NewAVAXExecutor(bscClient *ethclient.Client, swapAddr string, config *util.Config) *AvaxExecutor {
	agentAbi, err := abi.JSON(strings.NewReader(agent.BSCSwapAgentABI))
	if err != nil {
		panic("marshal abi error")
	}

	avaxSwapAgentInst, err := contractabi.NewETHSwapAgent(ethcmm.HexToAddress(swapAddr), bscClient)
	if err != nil {
		panic(err.Error())
	}

	return &AvaxExecutor{
		Chain:             common.ChainAVAX,
		Config:            config,
		SwapAgentAddr:     ethcmm.HexToAddress(swapAddr),
		AVAXSwapAgentInst: avaxSwapAgentInst,
		SwapAgentAbi:      agentAbi,
		Client:            bscClient,
	}
}

func (e *AvaxExecutor) GetChainName() string {
	return e.Chain
}

func (e *AvaxExecutor) GetBlockAndTxEvents(height int64) (*common.BlockAndEventLogs, error) {
	ctxWithTimeout, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	header, err := e.Client.HeaderByNumber(ctxWithTimeout, big.NewInt(height))
	if err != nil {
		return nil, err
	}

	childHeader, err := e.Client.HeaderByNumber(ctxWithTimeout, big.NewInt(height+1))
	if err != nil {
		return nil, err
	}

	packageLogs, err := e.GetLogs(childHeader)
	if err != nil {
		return nil, err
	}

	return &common.BlockAndEventLogs{
		Height:          height,
		Chain:           e.Chain,
		BlockHash:       childHeader.ParentHash.String(),
		ParentBlockHash: header.ParentHash.String(),
		BlockTime:       int64(header.Time),
		Events:          packageLogs,
	}, nil
}
func (e *AvaxExecutor) GetLogs(header *types.Header) ([]interface{}, error) {
	return e.GetSwapStartLogs(header)
}

func (e *AvaxExecutor) GetSwapStartLogs(header *types.Header) ([]interface{}, error) {
	topics := [][]ethcmm.Hash{{AVAX2BSCSwapStartedEventHash}}

	blockHash := header.ParentHash
	// util.Logger.Infof("%s", blockHash.String())

	ctxWithTimeout, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// util.Logger.Infof("kek! %s", blockHash)
	logs, err := e.Client.FilterLogs(ctxWithTimeout, ethereum.FilterQuery{
		BlockHash: &blockHash,
		Topics:    topics,
		Addresses: []ethcmm.Address{e.SwapAgentAddr},
	})
	if err != nil {
		return nil, err
	}

	eventModels := make([]interface{}, 0, len(logs))
	for _, log := range logs {
		event, err := ParseAVAX2BSCSwapStartEvent(&e.SwapAgentAbi, &log)
		if err != nil {
			util.Logger.Errorf("parse event log error, er=%s", err.Error())
			continue
		}
		if event == nil {
			continue
		}

		eventModel := event.ToSwapStartTxLog(&log)
		eventModel.Chain = e.Chain
		util.Logger.Debugf("Found AVAX2BSC swap, txHash: %s, token address: %s, amount: %s, fee amount: %s",
			eventModel.TxHash, eventModel.TokenAddr, eventModel.Amount, eventModel.FeeAmount)
		eventModels = append(eventModels, eventModel)
	}
	return eventModels, nil
}
